<?php 
//Delete item.
if ( isset($_POST['path']) && file_exists($_POST['path']) ) {
    $path = $_POST['path'];
    unlink($path);
    echo '<div class="uploaded">Your file deleted <i class="fas fa-check-circle"></i></div>';
    include_once("../view/main.php");
} else {
    include_once("../view/main.php");
}
?>