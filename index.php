<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/variables.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />
</head>

<body>
    <section class="section">
        <div class="container">
            <form enctype="multipart/form-data" class="px-3" id="upload-form">
                <label for="file" class="button"><i class="fas fa-cloud-upload-alt"></i>Choose your image</label>
                <input type="file" name="file" id="file" />
                <button type="submit" name="submit" class="submit"><i class="fas fa-plus"></i>Add image</button>
            </form>
            <div class="content">
                <?php
                    //include site content and call function directory 
                    include_once("view/content.php"); 
                    directory("images_250/"); 
                ?>
            </div>
        </div>
    </section>
    <script src="js/index.js"></script>
    <script src="js/pagination.js"></script>
</body>

</html>