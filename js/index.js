//When is click on the browser back or forward buttons will work onpopstate function
window.onpopstate = function() {
	var url = new URL(window.location.href);
	var page = url.searchParams.get('currentPage');
	if (document.querySelector('.pagination')) {
		let links = document.querySelectorAll('.page-item');
		if (page > links.length - 2) {
			console.log('Not');
		} else {
			const xhr = new XMLHttpRequest();
			document.querySelector('.content').classList.add('content-load');
			xhr.open('POST', 'view/main.php', true);
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send(`page=${page}`);
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && xhr.status == 200) {
					document.querySelector('.content').classList.remove('content-load');
					document.querySelector('.content').innerHTML = this.responseText;
					document.querySelectorAll('.page-link')[page].focus();
				}
			};
		}
	}
};

//Upload file functional
if (document.querySelector('#upload-form')) {
	const inputFile = document.querySelector('#file');
	const uploadForm = document.querySelector('#upload-form');
	uploadForm.onsubmit = function(e) {
		e.preventDefault();
		var url_string = window.location.href;
		var url = new URL(url_string);
		var page = url.searchParams.get('currentPage');
		var file = document.querySelector('#file').files[0];
		var form_data = new FormData();
		form_data.append('file', file);
		if (page == null) {
			page = 1;
		}
		form_data.append('currentPage', page);
		window.history.pushState({ page: page }, `pageCount${page}`, `${location.pathname}?currentPage=${page}`);
		if (file !== undefined) {
			const xhr = new XMLHttpRequest();
			xhr.open('POST', 'upload/upload.php', true);
			xhr.setRequestHeader('Cache-Control', 'no-cache');
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xhr.send(form_data);
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && xhr.status == 200) {
					document.querySelector('.content').innerHTML = this.responseText;
					if (document.querySelectorAll('.page-link')[page]) {
						document.querySelectorAll('.page-link')[page].focus();
					}
				}
			};
		}
	};
}
//Delete file functional.
delate = (element, e) => {
	e.preventDefault();
	let href;
	if (window.location.search) {
		href = element.search.replace('?', '&');
	} else {
		href = element.search;
	}
	var url_string = window.location.href + href;
	var url = new URL(url_string);
	var path = url.searchParams.get('path');
	var page = url.searchParams.get('currentPage');
	if (document.querySelectorAll('.card').length == 1) {
		if (page > 1) {
			page -= 1;
		}
	}
	if (page == null) {
		page = 1;
	}
	window.history.pushState({ page: page }, `pageCount${page}`, `${location.pathname}?currentPage=${page}`);
	const xhr = new XMLHttpRequest();
	xhr.open('POST', 'action/delete.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send(`path=${path}&currentPage=${page}`);
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && xhr.status == 200) {
			document.querySelector('.content').innerHTML = this.responseText;
			if (document.querySelectorAll('.page-link')[page]) {
				document.querySelectorAll('.page-link')[page].focus();
			}
		}
	};
};
//rename button functional. When click will open form rename
var handle_rename;
var old_name;
var new_image;
var old_href;
rename = (element, e) => {
	e.preventDefault();
	let allForms = document.querySelectorAll('.form-rename');
	allForms.forEach(function(elem) {
		elem.style.display = 'none';
	});
	e.path.forEach(function(elem) {
		if (elem.className == 'card') {
			for (let cardChild of elem.children) {
				if (cardChild.className == 'card-img-top') {
					new_image = cardChild;
				}
				if (cardChild.className == 'card-body') {
					for (let cardBodyChild of cardChild.children) {
						if (cardBodyChild.className == 'handle-rename') {
							handle_rename = cardBodyChild;
						}
						if (cardBodyChild.className == 'form-rename') {
							cardBodyChild.style.display = 'block';
							cardBodyChild.children[0].focus();
						}
						if (cardBodyChild.className == 'card-title') {
							old_name = cardBodyChild;
						}
					}
				}
				if (cardChild.className == 'card-footer') {
					for (let cardFooterChild of cardChild.children) {
						old_href = cardFooterChild;
					}
				}
			}
		}
	});
};
//Render functional for rename
function render(response) {
	handle_rename.classList.remove('erorr-rename');
	handle_rename.classList.add('change-rename');
	handle_rename.innerHTML = 'Your file renamed';
	old_name.innerHTML = response;
	new_image.src = `images_250/${response}`;
	let href = old_href.href.replace(`${old_href}`, `?path=../images_250/${response}`);
	old_href.href = href;
}
//Form rename functional
form_rename = (element, e) => {
	e.preventDefault();
	var newName = element[0].value;
	var oldName = old_name.innerText.slice(0, -4);
	var type = old_name.innerText.slice(-4);
	var incorrect = [ ' ', '/', '*', '"', '#', '%', '&', '|', '\\' ];
	var newNameArray = new Array();
	for (let i = 0; i < newName.length; i++) {
		if (incorrect.indexOf(newName.slice(i, i + 1)) != '-1') {
			newNameArray = false;
			break;
		} else {
			newNameArray.push(newName.slice(i, i + 1));
		}
	}
	if (newNameArray == false) {
		handle_rename.classList.remove('change-rename');
		handle_rename.classList.add('erorr-rename');
		handle_rename.innerText = `can't write these /, *, ", #, %, &, |, ${'\\'}.`;
	} else {
		const xhr = new XMLHttpRequest();
		xhr.open('POST', 'action/rename.php', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send(`oldName=${oldName}&newName=${newName}&type=${type}`);
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && xhr.status == 200) {
				render(this.responseText);
				element.style.display = 'none';
				if (document.querySelector('.uploaded')) {
					document.querySelector('.uploaded').parentNode.removeChild(document.querySelector('.uploaded'));
				}
				if (document.querySelector('.error-upload')) {
					document
						.querySelector('.error-upload')
						.parentNode.removeChild(document.querySelector('.error-upload'));
				}
			}
		};
	}
};
