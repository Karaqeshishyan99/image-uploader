//Pagination functional
pagination = (element, e) => {
	e.preventDefault();
	let href;
	if (window.location.search) {
		href = element.search.replace('?', '&');
	} else {
		href = element.search;
	}
	var url_string = window.location.href + href;
	var url = new URL(url_string);
	var page = url.searchParams.get('page');
	if (page == 'min' || page == 'max') {
		console.log('Not');
	} else {
		element.classList.add('active');
		const xhr = new XMLHttpRequest();
		document.querySelector('.content').classList.add('content-load');
		xhr.open('POST', 'view/main.php', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send(`page=${page}`);
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && xhr.status == 200) {
				document.querySelector('.content').classList.remove('content-load');
				document.querySelector('.content').innerHTML = this.responseText;
				document.querySelectorAll('.page-link')[page].focus();
				window.history.pushState(
					{ page: page },
					`pageCount${page}`,
					`${location.pathname}?currentPage=${page}`
				);
			}
		};
	}
};
