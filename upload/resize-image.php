<?php
//With this resize_image function we resize it by making it 250x250.
function resize_image ( $target, $newcopy, $newWidth, $newHeight, $ext ) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    $img = "";
    if ($ext == "gif" || $ext == "GIF"){
        $img = imagecreatefromgif($target);
    } else if ( $ext == "png" || $ext == "PNG" ){
        $img = imagecreatefrompng($target);
    } else {
        $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $newWidth, $newHeight, $w_orig, $h_orig);
    if ($ext == "gif" || $ext == "GIF"){
        imagegif($tci, $newcopy);
        header('Content-Type: image/gif');
    } else if ( $ext == "png" || $ext == "PNG" ){
        imagepng($tci, $newcopy);
        header('Content-Type: image/png');
    } else {
        imagejpeg($tci, $newcopy);
        header('Content-Type: image/jpeg');
    }
    imagedestroy($tci);
}
?>