<?php
//File Upload
if ( isset($_FILES['file']) ) {
    if (!file_exists('../images')) {
        mkdir('../images', 0666, true);
    }
    $folderDir = '../images/';
    $folderDirResize = '../images_250/';
    $fileName = $_FILES['file']['name'];
    $fileType = $_FILES['file']['type'];
    $fileSize = $_FILES['file']['size'];
    $fileTmp = $_FILES['file']['tmp_name'];
    $fileError = $_FILES['file']['error'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strToLower(end($fileExt));
    $alLowed = array('jpg', 'jpeg', 'png', 'gif');
    if ( in_array( $fileActualExt, $alLowed ) ) {
        if ( $fileError === 0 ) {
            if ( $fileSize < 1 * 1024 * 1024 ) {
                $imageName = strToLower(reset($fileExt));
                $file_name;
                //checking if file name already exists if yes we're adding a number.
                if ( file_exists($folderDirResize.$fileName) ) {
                    if ( file_exists($folderDirResize.$imageName.'_1.'.$fileActualExt) ) {
                        for ( $i = 1; file_exists($folderDirResize.$imageName.'_'.$i.'.'.$fileActualExt) == true; $i++ ) {  
                            $file_ = explode('_', $imageName.'_'.$i.'.'.$fileActualExt);
                            $file_end = strToLower(end($file_));
                            $file_1 = explode('.', $file_end);
                            $file_reset = strToLower(reset($file_1));
                            $imageCount = $file_reset + 1;
                            $file_name = $imageName . '_'.$imageCount.'.'.$fileActualExt;
                        }
                    } else {
                        $file_name = $imageName . '_1.'.$fileActualExt;
                    }
                } else {
                    $file_name = $imageName .'.'.$fileActualExt;
                }
                $fileUpload = $folderDir . $file_name;
                move_uploaded_file($fileTmp, $fileUpload);
                
                if (!file_exists('../images_250')) {
                    mkdir('../images_250', 0777, true);
                }
                include_once("resize-image.php");
                $target_image = $fileUpload;
                $resized_image = $folderDirResize.$file_name;
                resize_image($target_image, $resized_image, 250, 250, $fileActualExt);
                echo '<div class="uploaded">Your file uploaded <i class="fas fa-check-circle"></i></div>';
                include_once("../view/main.php");
            } else {
                echo '<div class="error-upload">Your file is big <i class="fas fa-times-circle"></i></div>';
                include_once("../view/main.php");
            }
        } else {
            echo '<div class="error-upload">There was error uploading your file <i class="fas fa-times-circle"></i></div>';
            include_once("../view/main.php");
        }
    } else {
        echo '<div class="error-upload">You cannot upload files of this type <i class="fas fa-times-circle"></i></div>';
        include_once("../view/main.php");
    }
} else {
    include_once("../view/main.php");
}
?>