<?php 
//With this card function we get the one file item.
function card($dir, $file) {
    return '
        <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-4">
            <div class="card">
                <img class="card-img-top" src=images_250/'.$file.' alt="Card image cap">
                <div class="card-body">
                    <div class="handle-rename"></div>
                    <form onsubmit="form_rename(this, event)" class="form-rename" enctype="multipart/form-data">
                        <input type="text" name="newName" class="input-rename" placeholder="new name" />
                        <button type="submit" name="submit" class="submit submit-rename">Change</button>
                    </form>
                    <h5 class="card-title">'.$file.'</h5>
                    <p class="card-text"><b>Dir name:</b> '.pathinfo($dir . $file, PATHINFO_DIRNAME).'</p>
                    <p class="card-text"><b>File size:</b> '.formatSizeUnits(filesize($dir . $file)).'</p>
                    <p class="card-text"><b>File type:</b> '.mime_content_type($dir . $file).'</p>
                </div>
                <div class="card-footer">
                    <div class="button rename" onclick="rename(this, event)">Rename</div>
                    <a class="button delate" onclick="delate(this, event)" href="?path='.$dir . $file.'">Delate</a>
                </div>
            </div>
        </div>
    ';
}
?>