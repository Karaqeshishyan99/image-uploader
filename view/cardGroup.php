<div class="card-group mt-5">
    <?php
    //Get all files.
    if ( is_dir($dir) ) {
        if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){
                if ( $file != "." && $file != ".." ) {
                    $int++;
                    $start;
                    $end = $int <= $page * $limit;
                    if ( $page == 1 ) {
                        $start = $page;
                    } else {
                        $start = $int > ($page - 1) * $limit;
                    }
                    if ( $end && $start ) {
                        echo card($dir, $file);
                    }
                }
            }
            closedir($dh);
        }
    }
?>
</div>