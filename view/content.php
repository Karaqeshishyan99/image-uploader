<?php 
//With this directory function we get the directory parametr.
function directory ($directory) {
    $limit = 4;
    $int = 0;
    $dir = $directory;
    if ( file_exists($dir) ) {
        $fi = new FilesystemIterator($dir, FilesystemIterator::SKIP_DOTS);
        $fileCount = iterator_count($fi);
    } else {
        $fileCount = 0;
    }
    $pages = ceil($fileCount / $limit);
    include_once("bytes.php");
    $page = 1;
    if ( isset($_POST['page']) ) {
        $page = $_POST['page'];
    } else if ( isset($_REQUEST['currentPage']) ) {
        $page = $_REQUEST['currentPage'];
    }
    include_once("pagination.php");
    include_once("card.php");
    include_once('cardGroup.php');
}
?>