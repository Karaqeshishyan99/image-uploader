<?php
//With this pageItem function we get the one page item.
function pageItem($i) {
    return '<li class="page-item"><a class="page-link" onclick="pagination(this, event)" href="?page='.($i + 1).'">'.($i + 1).'</a></li>';
}
//With this page function we get the all page items.
function page($pages) {
    $links = '';
    for ( $i = 0; $i < $pages; $i++ ) {
        $links .= pageItem($i);
    }
    return $links;
}
//With this pagination function we get the pagination compononet.
function pagination($min, $max, $pages) {
    return '
    <nav class="navigation-pagination" aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" onclick="pagination(this, event)" href="?page='.$min.'"
                    aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            '.page($pages).'
            <li class="page-item">
                <a class="page-link" onclick="pagination(this, event)" href="?page='.$max.'" aria-label=" Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
    ';
}
//Get page count.
if ( $pages > 1 ) {
    if ( $page == 1 ) {
        $min = 'min';
    } else {
        $min = $page - 1;
    }  
    if ( $page >= $pages ) {
        $max = 'max';
    } else {
        $max = $page + 1;
    }
    echo pagination($min, $max, $pages);
}
?>